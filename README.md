<div align="center">

 # TynixCloud project :: Translations

 <a href="https://gitlab.com/tynixcloud-public/tynixcloud-localization/-/blob/main/LICENSE">
   <img src="https://img.shields.io/github/license/whilein/nmslib">
 </a>

 ***
Данный репозиторий используется для мульти-язяковой системы с проекта **TynixCloud Project (mc.tynixcloud.ru)**
</div>

---
## Обратная связь
Если у Вас есть какие-то предложения или желание в помощи по переводу, 
то можете обратиться к нам через указанные контакты:

* **[Discord](https://tynixcloud.ru/discord)**
* **[ВКонтакте](https://vk.me/tynixcloud)**
